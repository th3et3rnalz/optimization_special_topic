from mnist_setup import setup_nn_mnist_classification, loss, accuracy, one_hot
from jax import jit, value_and_grad
import matplotlib.pyplot as plt
import optax
import jax.numpy as np
import time


def setup_and_learn(num_epochs=10, batch_dim=128, hidden_dim=512, step_size=1e-3, use_wandb=False, debug=True,
                    dataset_proportion: float = 1.0, optimizer="adam"):
    if use_wandb:
        import wandb

        wandb.init(project="mnist_classification")
        wandb.config = {"batch_dim": batch_dim,
                        "hidden_dim": hidden_dim,
                        "num_epochs": num_epochs,
                        "step_size": step_size}

    if debug:
        print(f"=== Just started new run: {num_epochs=} = {batch_dim=} ===")

    train_loader, test_loader, initial_params, key = setup_nn_mnist_classification(batch_dim=batch_dim,
                                                                                   hidden_dim=hidden_dim,
                                                                                   dataset_proportion=dataset_proportion)

    if optimizer == "adam":
        optimizer = optax.adam(step_size)
    elif optimizer == "sgd":
        optimizer = optax.sgd(step_size)

    opt_state_initial = optimizer.init(initial_params)

    @jit  # Adam optimizer
    def update(w_b, x, y, opt_state):
        """Compute the gradient for a batch and update the parameters"""
        value, grads = value_and_grad(loss)(w_b, x, y)
        updates, opt_state = optimizer.update(grads, opt_state)
        params = optax.apply_updates(w_b, updates)
        return params, opt_state, value

    # ============================================================================================
    # ============================= OPTIMIZING WEIGHTS AND BIASES ================================
    # ============================================================================================
    def run_mnist_training_loop(n_epochs, opt_state, w_b, net_type="MLP"):
        log_acc_train = []
        log_acc_test = []
        train_loss = []
        epoch_time = []

        # Saving the accuracy of a randomly initialized set of parameters.
        start_time = time.time()
        train_acc = accuracy(w_b=w_b, data_loader=train_loader)
        test_acc = accuracy(w_b=w_b, data_loader=test_loader)
        log_acc_train.append(train_acc)
        log_acc_test.append(test_acc)

        if use_wandb:
            wandb.log({"train_acc": train_acc,
                       "test_acc": test_acc,
                       "epoch_time": time.time() - start_time},
                      step=-1)

        if debug:
            print(
                f"Epoch {0} | T: {time.time() - start_time:0.2f} | A_test: {test_acc:0.3f} | A_train: {train_acc:0.6f}")

        for epoch in range(n_epochs):
            start_time = time.time()

            for batch_idx, (data, target) in enumerate(train_loader):
                if net_type == "MLP":
                    x = np.array(data).reshape(data.size(0), 28 * 28)
                else:
                    raise NotImplementedError
                y = one_hot(np.array(target), 10)
                w_b, opt_state, loss_v = update(w_b, x, y, opt_state)
                train_loss.append(loss_v)
                if use_wandb:
                    wandb.log({"train_loss": loss_v}, step=epoch * len(train_loader) + batch_idx)

            print("Performed the training, now evaluating the accuracy.")
            # train_acc = accuracy(w_b=w_b, data_loader=train_loader)
            test_acc = accuracy(w_b=w_b, data_loader=test_loader)
            epoch_t = time.time() - start_time

            log_acc_train.append(train_acc)
            log_acc_test.append(test_acc)
            epoch_time.append(epoch_t)

            if use_wandb:
                wandb.log({"train_acc": train_acc,
                           "test_acc": test_acc,
                           "epoch_time": epoch_t},
                          step=epoch * len(train_loader) + batch_idx)
            print(f"Epoch {epoch + 1} | T: {epoch_t:0.2f} | A_test: {test_acc:0.3f} | A_train: {train_acc:0.6f}")

        return train_loss, log_acc_train, log_acc_test, epoch_time

    final_train_loss, final_train_acc, final_test_acc, t_epoch = run_mnist_training_loop(n_epochs=num_epochs,
                                                                                         opt_state=opt_state_initial,
                                                                                         w_b=initial_params,
                                                                                         net_type="MLP")

    return final_train_loss, final_train_acc, final_test_acc, t_epoch


if __name__ == "__main__":

    data = setup_and_learn(num_epochs=5, dataset_proportion=1)

    plt.subplot(2, 1, 1)
    x = np.linspace(0, 5, len(data[0]))
    plt.plot(x, data[0], label="Training loss")
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.plot(data[1], label="Train accuracy")
    plt.plot(data[2], label="Test accuracy")
    plt.legend()

    plt.show()
