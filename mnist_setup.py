import matplotlib.pyplot as plt
from jax.scipy.special import logsumexp
from jax import random, numpy as np, vmap, nn
from random import randint
from torchvision import datasets, transforms
import numpy as onp
import torch
import os

# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.

os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'


def relu_layer(w_b, x):
    return nn.relu(np.dot(w_b[0], x) + w_b[1])


def plot_examples(data_loader):
    # Plot some MNIST example samples
    for batch_idx, (data, target) in enumerate(data_loader):
        images = onp.array(data[:4, ...]).reshape(4, 28, 28)
        target = onp.array(target[:4, ...])
        break

    fig, axs = plt.subplots(1, 4, figsize=(10, 5))
    for i, ax in enumerate(axs.flatten()):
        ax.imshow(images[i, ...], cmap="Greys")
        ax.set_title("Label: {}".format(target[i]), fontsize=30)
        ax.set_axis_off()
    fig.tight_layout()
    plt.show()


def get_mnist_dataloaders(batch_dim, proportion: float = 1.0):
    training_dataset = datasets.MNIST("data/",
                                      train=True,
                                      download=True,
                                      transform=transforms.Compose([
                                          transforms.ToTensor(),
                                          transforms.Normalize((0.1307,), (0.3081,))
                                      ]))
    test_dataset = datasets.MNIST("data/",
                                  train=False,
                                  download=True,
                                  transform=transforms.Compose([
                                      transforms.ToTensor(),
                                      transforms.Normalize((0.1307,), (0.3081,))
                                  ]))

    if proportion != 1.0:
        subset_train = range(int(60 * 1000 * proportion))
        training_dataset = torch.utils.data.Subset(training_dataset, subset_train)
        subset_test = range(int(10 * 1000 * proportion))
        test_dataset = torch.utils.data.Subset(test_dataset, subset_test)

    train_loader = torch.utils.data.DataLoader(
        training_dataset,
        batch_size=batch_dim,
        shuffle=True
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=batch_dim,
        shuffle=True
    )

    return train_loader, test_loader


def initialize_nn_wb(sizes, key):
    keys = random.split(key, len(sizes))

    def initialize_layer(m, n, key, scale=1e-2):
        w_key, b_key = random.split(key)
        weight_matrix = scale * random.normal(w_key, (n, m))
        bias_vector = scale * random.normal(b_key, (n,))

        return weight_matrix, bias_vector

    params = []
    for i in range(len(sizes) - 1):
        m = sizes[i]
        n = sizes[i + 1]
        params.append(initialize_layer(m=m, n=n, key=keys[i]))

    return params


def setup_nn_mnist_classification(batch_dim: int, hidden_dim: int, dataset_proportion: float = 1.0):
    key = random.PRNGKey(randint(-10000, 10000))

    layer_sizes = [28 * 28, hidden_dim, hidden_dim, 10]

    train_loader, test_loader = get_mnist_dataloaders(batch_dim=batch_dim,
                                                      proportion=dataset_proportion)

    # ========================== INITIALIZE PARAMETERS NEURAL NETWORK ============================
    params = initialize_nn_wb(sizes=layer_sizes, key=key)

    return train_loader, test_loader, params, key


def forward_pass(params, x_0):
    x = x_0

    for w, b in params[: -1]:
        x = relu_layer([w, b], x)

    final_w, final_b = params[-1]
    log_its = np.dot(final_w, x) + final_b
    # return nn.softmax(log_its).val[0, :]  # CHANGES MADE HERE !!!!!!!!!!!!!!!!!!
    return log_its - logsumexp(log_its)


batch_forward = vmap(forward_pass, in_axes=(None, 0), out_axes=0)


def one_hot(x, k, dtype=np.float32):
    """Create a one-hot encoding of x of size k"""
    return np.array(x[:, None] == np.arange(k), dtype)


def loss(w_b, x, y):
    """Compute the multiclass cross-entropy loss"""
    y_hat = batch_forward(w_b, x)
    return -np.sum(y * y_hat) / x.shape[0]   # + 0.001*theta_norm


def accuracy(w_b, data_loader):
    """Compute the accuracy over an entire data_loader"""
    acc_total = 0
    for batch_idx, (data, target) in enumerate(data_loader):
        images = np.array(data).reshape(data.shape[0], 28 * 28)
        targets = one_hot(np.array(target), 10)

        target_class = np.argmax(targets, axis=1)
        predicted_class = np.argmax(batch_forward(w_b, images), axis=1)
        acc_total += np.sum(predicted_class == target_class)

    return acc_total / len(data_loader.dataset)
