from mnist_classification import setup_and_learn
from multiprocessing import Pool
from optax import sgd, adam, rmsprop
import numpy as np
import pickle

n_epochs = 10


def train_it(data):
    optim, i = data
    i = str(i)
    if optim == "sgd":
        optimizer = sgd(0.001)
    elif optim == "nesterov":
        optimizer = sgd(0.001, momentum=0.9, nesterov=True)
    elif optim == "rmsprop":
        optimizer = rmsprop(learning_rate=0.001)
    elif optim == "adam":
        optimizer = adam(learning_rate=0.001)
    else:
        raise NotImplementedError

    data = setup_and_learn(num_epochs=n_epochs,
                           batch_dim=1024,
                           optimizer=optimizer,
                           dataset_proportion=1)
    final_train_loss, final_train_acc, final_test_acc, t_epoch = data
    data_dict = {i+"_train_loss": np.array(final_train_loss),
                 i+"_test_acc": np.array(final_test_acc),
                 i+"_t_epoch": np.array(t_epoch)}
    return data_dict


pool = Pool(processes=4)
n_repeats = 8

for optim in ["adam"]:  # "sgd", "nesterov"]:
    print("\n\n")
    print("=========================")
    print(f"=== {optim} ===")
    print("=========================")
    work = list(zip([optim] * n_repeats, range(n_repeats)))
    res = pool.map(train_it, work)

    data_dict = {}
    print(res)
    for d in res:
        data_dict = data_dict | d
    with open(f"data/{optim}_extr.pkl", 'wb') as f:
        pickle.dump(data_dict, f)
