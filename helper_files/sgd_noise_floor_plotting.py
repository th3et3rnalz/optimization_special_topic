import os
import pickle
import statistics
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# with open("../sgd_noise_floor.txt", 'r') as f:
#     data = f.readlines()

# data = [line.replace("]", "").replace("[", "") for line in data[0].split("][")][-2:]
# data = [line.split(', ') for line in data]

# data = [line.split(" - ") data[-2:]]
# b, d_16 = data[-2].split(" - ")
# assert int(b) == 16
#
# b, d_256 = data[-1].split(" - ")
# assert int(b) == 256
#
# d_16 = eval(d_16)
# d_256 = eval(d_256)

# for b_i, d_i in [line.split(" - ") for line in data[-2:]]:
#     window_size = 10
#
#     b_i = int(b_i)
#     d_i = eval(d_i)
#
#     d = pd.Series(d_i)
#     variance = d.rolling(window_size).var()
#     mean = d.rolling(window_size).mean()
#
#     # Let's now discard the value that have no mean or variance yet
#     variance = variance.to_numpy()[window_size:]*1000
#     mean = mean.to_numpy()[window_size:]
#
#     x = np.arange(mean.shape[0])*b_i
#
#     plt.fill_between(x, mean - variance, mean + variance)
#     plt.plot(x, mean, label=str(b_i))
#
# plt.legend()
# plt.yscale("log")
# plt.xscale("log")
# plt.show()
# features = data
# labels = [16, 32, 64, 128, 256]
# plt.loglog(np.arange(len(d_16))*16, d_16, label="16")
# plt.loglog(np.arange(len(d_256))*256, d_256, label="256")


# for i in range(len(data)):
#     d_i = [float(val) for val in data[i]]
#     # l_i = labels[i]
#
#     x = np.arange(len(d_i))*l_i
#     plt.loglog(x, d_i, label=str(l_i))

# plt.legend()
# plt.show()

# files = os.listdir("../data")
# window_size = 10
# smoothing_size = 2
# n_epochs = 10

# for file in files:
#     if file == "MNIST":
#         continue
#
#     batch_size = int(file.split("_")[0])
#
#     df = pd.read_pickle("../data/" + file)
#
#     if file.find("loss") != -1:
#         ax = plt.subplot(1, 2, 1)
#
#         df_smoothed = df.rolling(smoothing_size).mean()[smoothing_size:]
#         df = df[smoothing_size:]  # To ensure both dataframe have the same size
#
#         mean = df_smoothed.T.mean().to_numpy()[window_size:]
#         variance = np.sqrt(df.T.var().to_numpy()[window_size:])
#
#         x = np.arange(mean.shape[0]) * n_epochs/mean.shape[0]
#
#         ax.fill_between(x, mean-variance, mean+variance, alpha=0.2)
#         ax.plot(x, mean, label=str(batch_size))
#         ax.set_xlabel("Epoch")
#         ax.legend()
#
#     elif file.find("test_acc") != -1:
#         ax = plt.subplot(1, 2, 2)
#
#         mean = df.T.mean().to_numpy()
#         variance = df.T.var().to_numpy()
#
#         x = np.arange(mean.shape[0])
#
#         ax.fill_between(x, mean - variance, mean + variance, alpha=0.2)
#         ax.plot(x, mean, label=str(batch_size))
#         ax.set_xlabel("Epoch")
#
#         ax.set_ylabel("Training loss")
#         ax.legend()
#
#
# plt.tight_layout()
# plt.show()

n_epochs = 10  # CHANGE MANUALLY!!!!!!!

for file in ["64.pkl", "256.pkl"]:
    batch_size = int(file.split(".")[0])

    with open("../data/"+file, 'rb') as f:
        data = pickle.load(f)

    loss = []
    t_epochs = []
    test_acc = []

    for key in data.keys():
        n_run = key.split("_")[0]
        d_type = key[1 + len(n_run):]

        if d_type == "loss":
            loss.append(data[key])
        if d_type == "test_acc":
            test_acc.append(data[key])
        if d_type == "t_epoch":
            t_epochs.append(data[key])

    n_repeats = int(len(data.keys())/3)

    loss_mean = np.mean(np.vstack(loss), axis=0)
    loss_std = np.std(np.vstack(loss), axis=0)
    t_epoch_mean = np.mean(np.vstack(t_epochs), axis=0)
    t_epoch_std = np.std(np.vstack(t_epochs), axis=0)
    test_acc_mean = np.mean(np.vstack(test_acc), axis=0)*100
    test_acc_std = np.std(np.vstack(test_acc), axis=0)*100

    plt.subplot(1, 3, 1)
    # plt.title("Loss progression")
    x = np.linspace(0, n_epochs, len(loss[0]))
    plt.semilogy(x, loss_mean, label=str(batch_size))
    plt.ylabel("Loss [-]")
    # plt.ylim((2, 3))
    plt.fill_between(x, loss_mean-loss_std, loss_mean + loss_std, alpha=0.2)

    plt.subplot(1, 3, 2)
    # plt.title("Epoch time")
    x = np.linspace(0, n_epochs, len(t_epochs[0]))
    plt.plot(x, t_epoch_mean, label=str(batch_size))
    plt.fill_between(x, t_epoch_mean-t_epoch_std, t_epoch_mean+t_epoch_std, alpha=0.2)
    plt.ylabel("Epoch time [s]")

    plt.subplot(1, 3, 3)
    # plt.title("Test accuracy")
    x = np.linspace(0, n_epochs, len(test_acc[0]))
    plt.plot(x, test_acc_mean, label=str(batch_size))
    plt.fill_between(x, test_acc_mean - test_acc_std, test_acc_mean + test_acc_std, alpha=0.2)
    plt.ylim((10, 50))
    plt.ylabel("Test Accuracy [%]")
    plt.xlabel("Epoch")

    print(f"batch_size={batch_size} - Test_Acc={test_acc_mean[-1]} pm {test_acc_std[-1]}")

plt.legend()
plt.tight_layout()
plt.show()
