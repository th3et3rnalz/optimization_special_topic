import matplotlib.pyplot as plt
import numpy as np
import pickle
import os


boxplot_data = []
boxplot_titles = []
time_epoch_mean = []
time_epoch_std = []

files = os.listdir("../data/SGD_data")
files = [file for file in files if file.find("_") != -1]
files = sorted(files, key=lambda x: int(x.split("_")[0]))

for batch_size in [64, 256, 1024, 4096, 16384]:
    filename = "../data/SGD_data/" + str(batch_size) + ".pkl"

    with open(filename, 'rb') as f:
        data = pickle.load(f)

    test_acc = []
    t_epoch = []
    for key, values in data.items():
        if key.find("test_acc") != -1:
            test_acc.append(values[-1]*100)
        if key.find("t_epoch") != -1:
            t_epoch.append(values)

    epoch_data = np.vstack(t_epoch)
    t_epoch_mean = np.mean(epoch_data)
    t_epoch_std = np.std(epoch_data)

    time_epoch_mean.append(t_epoch_mean)
    time_epoch_std.append(t_epoch_std)
    boxplot_data.append(test_acc)
    boxplot_titles.append(str(batch_size))


plt.subplot(2, 1, 1)
plt.ylabel("Accuracy [%]")
plt.boxplot(boxplot_data, labels=boxplot_titles)

ax = plt.subplot(2, 1, 2)
time_epoch_mean = np.array(time_epoch_mean)
time_epoch_std = np.array(time_epoch_std)
labels = [64, 256, 1024, 4096, 16384]
ax.set_ylabel("Epoch time [s]")
# plt.bar(x=np.arange(len(labels)),
#         height=time_epoch_mean,
#         width=0.35,
#         tick_label=labels,
#         yerr=time_epoch_std)
x = np.arange(len(labels))
ax.plot(x, time_epoch_mean)
ax.fill_between(x, time_epoch_mean-time_epoch_std, time_epoch_mean + time_epoch_std, alpha=0.2)
ax.set_xticks(x)
ax.set_xticklabels([str(el) for el in labels])

plt.show()
