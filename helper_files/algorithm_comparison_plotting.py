import pickle
import matplotlib.pyplot as plt
import numpy as np

n_epochs = 10  # CHANGE MANUALLY!!!!!!!

for file in ["Adam.pkl", "RMSProp.pkl"]:  # , "Nesterov.pkl", "SGD.pkl"]:
    name = file.split(".")[0]

    with open("../data/"+file, 'rb') as f:
        data = pickle.load(f)

    loss = []
    t_epochs = []
    test_acc = []

    for key in data.keys():
        n_run = key.split("_")[0]
        d_type = key[1 + len(n_run):]

        if key.find("train_loss") != -1:
            loss.append(data[key])
        if key.find("test_acc") != -1:
            test_acc.append(data[key])
        if key.find("t_epoch") != -1:
            t_epochs.append(data[key])

    n_repeats = int(len(data.keys())/3)

    loss_mean = np.mean(np.vstack(loss), axis=0)
    loss_std = np.std(np.vstack(loss), axis=0)
    t_epoch_mean = np.mean(np.vstack(t_epochs), axis=0)
    t_epoch_std = np.std(np.vstack(t_epochs), axis=0)
    test_acc_mean = np.mean(np.vstack(test_acc), axis=0)*100
    test_acc_std = np.std(np.vstack(test_acc), axis=0)*100

    plt.subplot(1, 2, 1)
    # plt.title("Loss progression")
    x = np.linspace(0, n_epochs, len(loss[0]))
    plt.semilogy(x, loss_mean, label=name)
    plt.ylabel("Loss [-]")
    plt.ylim((0.008, 4))
    plt.fill_between(x, loss_mean-loss_std, loss_mean + loss_std, alpha=0.2)

    plt.subplot(1, 2, 2)
    # plt.title("Test accuracy")
    x = np.linspace(0, n_epochs, len(test_acc[0]))
    plt.plot(x, test_acc_mean, label=name)
    plt.fill_between(x, test_acc_mean - test_acc_std, test_acc_mean + test_acc_std, alpha=0.2)
    plt.ylim((90, 100))
    plt.ylabel("Test Accuracy [%]")
    plt.xlabel("Epoch")

    print(f"Name={name} - Test_Acc={test_acc_mean[-1]} pm {test_acc_std[-1]}")

plt.legend()
plt.tight_layout()
plt.show()
