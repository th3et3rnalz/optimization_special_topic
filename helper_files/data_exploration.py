import matplotlib.pyplot as plt
import numpy as np
import pickle

with open("../data/adam_extr.pkl", 'rb') as f:
    data = pickle.load(f)

loss = []
test_acc = []
t_epoch = []

for key, value in data.items():
    if key.find("loss") != -1:
        loss.append(value)
    elif key.find("test_acc") != -1:
        test_acc.append(value)
    elif key.find("t_epoch"):
        t_epoch.append(value)


loss = np.vstack(loss)
mean_loss = np.mean(loss, axis=0)
std_loss = np.std(loss, axis=0)

plt.semilogy(mean_loss)
plt.show()

plt.plot(np.mean(np.vstack(test_acc), axis=0))
plt.ylim((0.9, 1))
plt.show()
