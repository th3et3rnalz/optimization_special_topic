from mnist_classification import setup_and_learn
from multiprocessing import Process


processes = []
for i in range(3):
    print("i")
    p = Process(target=setup_and_learn, kwargs={"use_wandb": False})
    processes.append(p)
    p.start()

[p.join() for p in processes]


